package de.ninesun;

import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;
import org.json.JSONObject;
import org.json.JSONArray;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

public class Watchdog {
  public static void main(String[] _args) {
    HashMap<String, String> argMap = new HashMap<>();
    HashMap<String, String> args = new HashMap<>();

    File f = new File(System.getProperty("java.class.path"));
    File dir = f.getAbsoluteFile().getParentFile();

    String workingDir = dir.toString();
    String configPath = "watchdog-config.json";

    try {
      argMap.put("conf", "VALUE");
      argMap.put("daemon", "FLAG");
      argMap.put("kill", "FLAG");
      ParseCLA parseCLA = new ParseCLA(_args);
      args = parseCLA.parse(argMap);
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return;
    }

    if (args != null && args.get("conf") != null) {
      configPath = args.get("conf");
    }

    JSONObject config;

    try {
      String text = new String(Files.readAllBytes(Paths.get(workingDir + "/" + configPath)), StandardCharsets.UTF_8);
      config = new JSONObject(text);
    } catch (Exception e) {
      System.out.println("Could not read from config at \"" + workingDir + "/" + configPath + "\"");
      System.out.println("Aborting.");
      e.printStackTrace();
      return;
    }

    try {
      boolean isDaemon = args != null && args.get("daemon") != null;
      boolean isKilling = args != null && args.get("kill") != null;

      Runner rn = new Runner(configPath, config, isDaemon, isKilling);

      if (args != null && args.get("kill") != null) {
        System.out.println("Killing all of them!");
        rn.kill();
        return;
      }

      rn.manage();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

class Runner {

  private String configPath;
  private JSONObject config;
  private boolean daemon;
  private Logger logger;

  Runner(String _configPath, JSONObject _config, boolean _daemon, boolean kill) throws Exception {
    config = _config;
    daemon = _daemon;
    configPath = _configPath;

    if (!kill && daemon) {
      logger = new Logger(config.getString("log") + "/daemon.log");
    } else {
      logger = new Logger();
    }
  }

  List<VirtualMachineDescriptor> getIdFromName(String name) {
    List<VirtualMachineDescriptor> vms = VirtualMachine.list();
    List<VirtualMachineDescriptor> result = new ArrayList<>();
    vms.forEach((v) -> {
      if (name == null || v.displayName().contains(name)) {
        result.add(v);
      }
    });
    return result;
  }

  void manage() throws Exception {
    if (config.has("daemonize")) {
      if (!daemon) {
        logger.log("daemonizing...");
        daemonize();
      }

      if (daemon && config.has("files")) {
        watch();
      }
    } else {
      if (config.has("files")) {
        watch();
      }
    }
  }

  void kill() throws Exception {
    ArrayList<Integer> pids = new ArrayList<>();


    // Get all subs

    // Kill all subs
    JSONArray files = config.getJSONArray("files");
    for (int i = 0; i < files.length(); i++) {
      BufferedReader input = null;
      try {
        String line;
        Process p = Runtime.getRuntime().exec("lsof -t " + files.getString(i));
        input = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = input.readLine()) != null) {
          pids.add(Integer.parseInt(line));
        }
      } catch (Exception err) {
        err.printStackTrace();
      } finally {
        if (input != null)
          input.close();
      }
    }

    // Get all watchdogs
    try {
      String line;
      Process p = Runtime.getRuntime().exec("lsof -t watchdog.jar");
      BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
      while ((line = input.readLine()) != null) {
        pids.add(Integer.parseInt(line));
      }
      input.close();
    } catch (Exception err) {
      err.printStackTrace();
    }

    Collections.sort(pids);

    pids.forEach(pid -> {
      try {
        logger.log("prepare to kill: " + pid);
        Runtime.getRuntime().exec("kill -2 " + pid);
      } catch (Exception ex) {
        logger.log("Could not kill." + ex.getMessage());
      }
    });
  }

  private void daemonize() {
    try {
      String path = Watchdog.class.getProtectionDomain().getCodeSource().getLocation().getPath();
      String decodedPath = URLDecoder.decode(path, "UTF-8");

      logger.log("Daemonizing \"" + decodedPath + "\"");
      logger.log("Output is logged to \"" + config.getString("log") + "/daemon.log\"");

      Runtime rt = Runtime.getRuntime();
      logger.log("Executing \"" + "java -jar " + decodedPath + " --conf " + configPath + " --daemon &\"");
      Process pr = rt.exec("java -jar " + decodedPath + " --conf " + configPath + " --daemon &\"");
    } catch (Exception e) {
      logger.log("Error while trying to daemonize!");
      logger.log(e.getMessage());
    }
  }

  private void watch() throws Exception {
    JSONArray arr = config.getJSONArray("files");
    HashMap<Integer, Runnable> runnables = new HashMap<>();
    for (int i = 0; i < arr.length(); i++) {
      runnables.put(i, new Runnable(arr.getString(i), null, logger));
    }

    while (true) {
      for (Map.Entry<Integer, Runnable> entry : runnables.entrySet()) {
        Runnable runnable = entry.getValue();

        File file = new File(runnable.getPath());
        if (file.lastModified() != runnable.getLastModified()) {
          logger.log("Restart due to update: " + runnable.getPath());
          runnable.getHandle().destroyForcibly();
          runnable.setHandle(null);
          runnable.setLastModified(file.lastModified());
        }

        if (runnable.getHandle() == null || !runnable.getHandle().isAlive()) {
          logger.log("starting: " + runnable.getPath());
          runnable.start();
        }

        entry.setValue(runnable);
      }

      try {
        Thread.sleep(5000);
      } catch (Exception ignored) {

      }
    }
  }
}

class Runnable {
  private String path;
  private Process handle;
  private Logger logger;
  private long lastModified;

  Runnable(String path, Process handle, Logger logger) {
    this.path = path;
    this.handle = handle;
    this.logger = logger;
    this.lastModified = new File(path).lastModified();
  }

  void start() {
    try {
      handle = Runtime.getRuntime().exec("java -jar " + path);
    } catch (Exception e) {
      logger.log(e.getMessage());
    }
  }

  String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  Process getHandle() {
    return handle;
  }

  public void setHandle(Process handle) {
    this.handle = handle;
  }

  public long getLastModified() {
    return lastModified;
  }

  public void setLastModified(long lastModified) {
    this.lastModified = lastModified;
  }
}

class ParseCLA {

  private String[] args;
  private HashMap<String, String> map;

  ParseCLA(String[] args) {
    this.args = args;
  }

  HashMap<String, String> parse(HashMap<String, String> options) throws Exception {
    HashMap<String, String> parsedOptions = new HashMap<>();

    for (int i = 0; i < args.length; i++) {
      String key = args[i];
      // Skip if String is a value (only --XXX)
      if (!key.substring(0, 2).equals("--")) {
        continue;
      }
      key = key.substring(2);

      String value = options.get(key);

      // Throw if argument is unknown
      if (value == null) {
        throw new Exception("Unknown argument \"" + key + "\"");
      }

      // Throw if argument has no value
      if (value.equals("VALUE") && i + 1 == args.length) {
        throw new Exception("No value given for argument \"" + key + "\"");
      }

      if (value.equals("FLAG")) {
        parsedOptions.put(key, "true");
      }

      if (value.equals("VALUE")) {
        parsedOptions.put(key, args[i + 1]);
      }
    }

    return parsedOptions;
  }
}

class Logger {

  private String output;

  Logger() {
  }

  Logger(String path) {
    output = path;
  }

  void log(int message) {
    log("" + message);
  }

  void log(String message) {
    log(message, false);
  }

  void log(String message, boolean forceCMD) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    String timestamp = sdf.format(new Date().getTime());

    if (forceCMD || output == null) {
      System.out.println(timestamp + " :   " + message);
    } else {
      BufferedWriter bw = null;
      FileWriter fw = null;

      try {
        File file = new File(output);

        if (!file.exists()) {
          file.createNewFile();
        }

        fw = new FileWriter(file.getAbsoluteFile(), true);
        bw = new BufferedWriter(fw);

        bw.write(timestamp + " :   " + message + '\n');

      } catch (Exception e) {
        System.out.println("Can't log output to file!");
      } finally {
        try {
          if (bw != null) {
            bw.close();
          }
          if (fw != null) {
            fw.close();
          }
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }
  }
}
